﻿using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public interface IPersonelRepository
    {
        Models.Personel Create(Models.Personel personel);
        Models.Personel Update(Models.Personel personel);
        bool Delete(int id);
        List<Models.Personel> GetAllPersonels();
        Models.Personel GetPersonelByEmail(string email);
        Models.Personel GetPersonelById(int id);
    }
}
