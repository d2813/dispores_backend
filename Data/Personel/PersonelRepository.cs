﻿using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public class PersonelRepository : IPersonelRepository
    {
        private readonly BaseContext _context;
        public PersonelRepository(BaseContext context)
        {
            _context = context;
        }
        public Personel Create(Personel personel)
        {
            try
            {
                _context.Personels.Add(personel);
                personel.Id = _context.SaveChanges();
            }
            catch(Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                personel = null;
            }
            
            return personel;
        }

        public bool Delete(int personel)
        {
            var per=_context.Personels.FirstOrDefault(p => p.Id == personel);
            _context.Personels.Remove(per);
            if (_context.SaveChanges() == -1) return false;
            return true;
        }

        public List<Personel> GetAllPersonels()
        {
            return _context.Personels.ToList();
        }

        public Personel GetPersonelByEmail(string email)
        {
            return _context.Personels.FirstOrDefault(p => p.Email == email);
        }
        public Personel GetPersonelById(int id)
        {
            return _context.Personels.FirstOrDefault(p => p.Id == id);
        }

        public Personel Update(Personel personel)
        {
            var res = _context.Personels.FirstOrDefault(p => p.Id == personel.Id);
            _context.Entry(res).CurrentValues.SetValues(personel);
            _context.SaveChanges();
            return res;
        }
    }
}
