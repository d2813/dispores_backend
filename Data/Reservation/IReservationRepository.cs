﻿using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public interface IReservationRepository
    {
        Reservation Create(Models.Reservation reservation);
        Reservation Update(Models.Reservation reservation);
        bool Remove(Models.Reservation reservation);
        List<Reservation> GetReservationEndToday();
        List<Reservation> GetAllReservations();
        Reservation GetReservationById(int id);
    }
}
