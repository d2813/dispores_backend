﻿using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly BaseContext _context;
        public ReservationRepository(BaseContext context)
        {
            _context = context;
        }

        public Models.Reservation Create(Models.Reservation reservation)
        {
            try
            {
                _context.Attach(reservation.Personel);
                _context.Attach(reservation.Resource);
                _context.Reservations.Add(reservation);
                reservation.Id = _context.SaveChanges();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                reservation = null;
            }

            return reservation;
        }

        public List<Reservation> GetAllReservations()
        {
            return _context.Reservations.ToList();
        }

        public Reservation GetReservationById(int id)
        {
            return _context.Reservations.FirstOrDefault(r => r.Id == id);
        }

        public List<Reservation> GetReservationEndToday()
        {
            return _context.Reservations.Where(r=>(r.DateFin.Day==DateTime.Today.Day&& r.DateFin.Month == DateTime.Today.Month && r.DateFin.Year == DateTime.Today.Year)).ToList();
        }

        public bool Remove(Reservation reservation)
        {
            throw new NotImplementedException();
        }

        public Reservation Update(Reservation reservation)
        {
            var res = _context.Reservations.FirstOrDefault(p => p.Id == reservation.Id);
            _context.Entry(res).CurrentValues.SetValues(reservation);
            _context.SaveChanges();
            return reservation;
        }
    }
}
