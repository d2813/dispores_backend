﻿using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public class ResourcesRepository : IResourcesRepository
    {
        private readonly BaseContext _context;
        public ResourcesRepository(BaseContext context)
        {
            _context = context;
        }

        public Resource Create(Resource resource)
        {
            try
            {
                _context.Resources.Add(resource);
                resource.Id = _context.SaveChanges();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                resource = null;
            }

            return resource;
        }

        public List<Resource> GetAllDispoResources()
        {
            return _context.Resources.Where(r=>r.Status==1).ToList();
        }

        public List<Resource> GetAllNonDispoResources()
        {
            return _context.Resources.Where(r => r.Status != 1).ToList();
        }

        public List<Resource> GetAllResources()
        {
            return _context.Resources.ToList();
        }

        public Resource GetResourceById(int id)
        {
            return _context.Resources.FirstOrDefault(p => p.Id == id);
        }

        public Resource GetResourceByNom(string nom)
        {
            return _context.Resources.FirstOrDefault(p => p.Nom == nom);
        }

        public List<Resource> GetResourceByType(string type)
        {
            return _context.Resources.Where(r => r.Type ==type).ToList();
        }

        public bool Remove(Resource resource)
        {
            try
            {
                _context.Resources.Remove(resource);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public Resource Update(Resource resource)
        {
            var res = _context.Resources.FirstOrDefault(p => p.Id == resource.Id);
            _context.Entry(res).CurrentValues.SetValues(resource);
            _context.SaveChanges();
            return res;
        }
    }
}
