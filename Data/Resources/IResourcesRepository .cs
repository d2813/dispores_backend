﻿using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public interface IResourcesRepository
    {
        Resource Create(Resource resource);
        Resource Update(Resource resource);
        bool Remove(Resource resource);
        List<Resource> GetAllResources();
        List<Resource> GetAllNonDispoResources();
        List<Resource> GetAllDispoResources();
        Resource GetResourceByNom(string nom);
        Resource GetResourceById(int id);
        List<Resource> GetResourceByType(string type);
    }
}
