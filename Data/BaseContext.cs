﻿using DispoRes.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Data
{
    public class BaseContext : DbContext
    {
        public BaseContext(DbContextOptions<BaseContext> contextOptions) : base(contextOptions)
        {

        }

        public DbSet<Personel> Personels { set; get; }
        public DbSet<Reservation> Reservations { set; get; }

        public DbSet<Resource> Resources { set; get; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Personel>()
       .HasMany(c => c.Reservations)
       .WithOne(e => e.Personel).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<Personel>(entity => { entity.HasIndex(e => e.Email).IsUnique(); });
            builder.Entity<Reservation>()
          .HasOne(e => e.Personel)
          .WithMany(c => c.Reservations);
            builder.Entity<Reservation>()
           .HasOne(e => e.Resource)
           .WithMany(c => c.Reservations);
            builder.Entity<Resource>()
       .HasMany(c => c.Reservations)
       .WithOne(e => e.Resource).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<Resource>(entity => { entity.HasIndex(e => e.Nom).IsUnique(); });
        }
    }
}
