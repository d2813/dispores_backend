﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DispoRes.Models
{
    public class Personel
    {

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }

        public string Email { get; set; }

        [JsonIgnore]public string Password { get; set; }

        public ICollection<Reservation> Reservations { get; set; }


    }
}
