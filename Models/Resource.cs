﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DispoRes.Models
{
    public class Resource
    {

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Type { get; set; }
        public int Status { get; set; }
        public ICollection<Reservation> Reservations { get; set; }

    }
}
