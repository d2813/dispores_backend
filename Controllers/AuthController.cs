﻿using Microsoft.AspNetCore.Mvc;
using System;
using DispoRes.Data;
using DispoRes.Dtos;
using DispoRes.Models;
using DispoRes.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Controllers
{
    [Route("api")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IPersonelRepository _repository;
        private readonly JwtService _jwtService;
        public AuthController(IPersonelRepository repository,JwtService jwtService)
        {
            _repository = repository;
            _jwtService = jwtService;
        }
        [HttpGet]
        public IActionResult Hello()
        {
            return Ok("Hello");
        }
        [HttpPost("register")]
        public IActionResult Register(RegisterDtos dtos)
        {
            var Personel = new Personel
            {
                Nom=dtos.Nom,
                Prenom=dtos.Prenom,
                Email=dtos.Email,
                Password=BCrypt.Net.BCrypt.HashPassword(dtos.Password)
            };
            Personel=_repository.Create(Personel);
            if(Personel==null) return BadRequest(new { message = "Personel Deja Exist" });
            return Created("succes",_repository.Create(Personel));
        }
        [HttpPost("login")]
        public IActionResult Login(LoginDtos dtos)
        {
            var per = _repository.GetPersonelByEmail(dtos.Email);
            if (per == null) return BadRequest(new { message = "Invalid Personel" });
            if(!BCrypt.Net.BCrypt.Verify(dtos.Password,per.Password)) return BadRequest(new { message = "Invalid Personel" });
            var jwt = _jwtService.Generate(per.Id);
            Response.Cookies.Append("jwt", jwt, new Microsoft.AspNetCore.Http.CookieOptions { HttpOnly = true });
            return Ok(new { message="success"});
        }
        [HttpDelete("remove")]
        public IActionResult Remove(IdDtos dtos)
        {
            var per = _repository.Delete(dtos.Id);
            if (!per) return BadRequest(new { message = "Invalid Personel" });
            return Ok(new { message = "success" });
        }
        [HttpGet("personel")]
        public IActionResult Personel()
        {
            try{
            var jwt = Request.Cookies["jwt"];
            var token = _jwtService.Verify(jwt);
            int persoId = int.Parse(token.Issuer);
            var perso = _repository.GetPersonelById(persoId);
            if (perso == null) return BadRequest(new { message = "Personel Not Exist" });
            return Ok(perso);
            }catch(Exception e)
            {
                return Unauthorized();
            }
        }
        [HttpGet("liste")]
        public IActionResult ListePersonel()
        {
            var list = _repository.GetAllPersonels();
            return Ok(list);
        }
        [HttpGet("logout")]
        public IActionResult Logout()
        {
            Response.Cookies.Delete("jwt");
            return Ok(new {
                message="success"
            });
        }
    }
}
