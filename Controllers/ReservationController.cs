﻿using Microsoft.AspNetCore.Mvc;
using DispoRes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispoRes.Dtos;
using DispoRes.Data;

namespace DispoRes.Controllers
{
    [Route("reservation")]
    [ApiController]
    public class ReservationController : Controller
    {
        private readonly IReservationRepository _Reservationrepository;
        private readonly IPersonelRepository _Personelrepository;
        private readonly IResourcesRepository _Resourcesrepository;


        public ReservationController(IPersonelRepository _Personelrepository, IResourcesRepository _Resourcesrepository, IReservationRepository _Reservationrepository)
        {
            this._Personelrepository = _Personelrepository;
            this._Resourcesrepository = _Resourcesrepository;
            this._Reservationrepository = _Reservationrepository;
        }
        [HttpPost("add")]
        public IActionResult Add(ReservationDtos reservationDtos)
        {
            var personel = _Personelrepository.GetPersonelById(reservationDtos.Personel);
            if(personel==null) return BadRequest(new { message = "Personel Not Exist" });
            var resourceList = _Resourcesrepository.GetResourceByType(reservationDtos.Resource);
            if (resourceList == null) return BadRequest(new { message = "Resource Not Exist" });
            var resource = resourceList.FirstOrDefault(r => r.Status == 1);
            if (resource == null) return BadRequest(new { message = "There is no "+reservationDtos.Resource+" available at the moment. We will let you know as soon as it is available" });
            var reservation = new Reservation
            {
                Personel = personel,
                Resource = resource,
                DateDebut = reservationDtos.DateDebut,
                DateFin = reservationDtos.DateFin
            };
            reservation = _Reservationrepository.Create(reservation);
            if (reservation == null) return BadRequest(new { message = "Error" });
            resource.Status = -1;
            resource.Reservations.Add(reservation);
            personel.Reservations.Add(reservation);
            
            return Created("succes", _Reservationrepository.Create(reservation));
        }
        [HttpGet("all")]
        public IActionResult GetAllResources()
        {
            var list = _Reservationrepository.GetAllReservations();
            return Ok(list);
        }
    }
}
