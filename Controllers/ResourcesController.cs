﻿using Microsoft.AspNetCore.Mvc;
using System;
using DispoRes.Data;
using DispoRes.Dtos;
using DispoRes.Models;
using DispoRes.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Controllers
{
    [Route("res")]
    [ApiController]
    public class ResourcesController : Controller
    {
        private readonly IResourcesRepository _repository;
        public ResourcesController(IResourcesRepository repository)
        {
            _repository = repository;
        }
        [HttpPost("add")]
        public IActionResult Create(ResDtos dtos)
        {
            var resource = new Resource
            {
                Nom = dtos.Nom,
                Type = dtos.Type,
                Status = dtos.Status
            };
            resource = _repository.Create(resource);
            if (resource == null) return BadRequest(new { message = "Resource Deja Exist" });
            return Created("succes", _repository.Create(resource));
        }
        [HttpPost("rem")]
        public IActionResult Remove(ResDtos dtos)
        {
            Resource resource = _repository.GetResourceByNom(dtos.Nom);
            if (resource == null) return BadRequest(new { message = "Resource Not Exist" });
            bool del = _repository.Remove(resource);
            if (!del) return BadRequest(new { message = "Resource Not Exist" });
            return Ok("succes");
        }
        [HttpGet("alldispo")]
        public IActionResult GetAllDispoResources()
        {
            var list = _repository.GetAllDispoResources();
            return Ok(list);
        }
        [HttpGet("allNotdispo")]
        public IActionResult GetAllNonDispoResources()
        {
            var list = _repository.GetAllNonDispoResources();
            return Ok(list);
        }
        [HttpGet("all")]
        public IActionResult GetAllResources()
        {
            var list = _repository.GetAllResources();
            return Ok(list);
        }
        [HttpPost("type")]
        public IActionResult GetAlltype(TypeDtos dtos)
        {
            var list = _repository.GetResourceByType(dtos.Type);
            return Ok(list);
        }
    }
}
