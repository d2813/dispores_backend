﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispoRes.Dtos
{
    public class ResDtos
    {
        public string Nom { get; set; }
        public string Type { get; set; }
        public int Status { get; set; }

    }
}
